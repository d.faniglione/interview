<?php

require_once 'vendor/autoload.php';
use Jobs\{ Candidates, Candidate, Vacancy };

$candidates = new Candidates($_SERVER['DOCUMENT_ROOT'] . 'candidates.json'); //Is there a "/" missing here?

//Find here three possible options in which cnadidates can be filtered

/**
 * OPTION 1: Following original example
 * This is less code, quicker to write, but it seems to be too specific and ad-hoc for particular scenarios.
*/

// get candidates that earn over than 40k
$vacancy = new Vacancy([40000]);

$earn_over_40k = [];
foreach ($candidates as $k => $v) {
	if ($v->candidate->salary > $vacancy->salaryRange[0]) {
		$earn_over_40k[] = $v;
	}
}
var_dump($earn_over_40k);

// get candidates that earn between 20 to 30k and know PHP or JS
$vacancy = new Vacancy([20000,30000],['php','js']);

$earn_between_20k_30k_php_js = [];
foreach ($candidates as $k => $v) {
	if ($v->candidate->salary > $vacancy->getMinSalary() 
			&& $v->candidate->salary < $vacancy->getMaxSalary()
			&& count(array_intersect($v->candidate->skills->languages, $vacancy->skills))>0) {
		$earn_between_20k_30k_php_js[] = $v;
	}
}
var_dump($earn_between_20k_30k_php_js);

// get candidates that earn less than 42k and know Ruby and C
$vacancy = new Vacancy([42000],['ruby','c']);

$earn_less_42k_ruby_c = [];
foreach ($candidates as $k => $v) {
	if ($v->candidate->salary < $vacancy->salaryRange[0]
			&& count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) == count($vacancy->skills)) {
		$earn_less_42k_ruby_c[] = $v;
	}
}
var_dump($earn_less_42k_ruby_c);

// get candidates that know go
$vacancy = new Vacancy([],['go']);

$only_go = [];
foreach ($candidates as $k => $v) {
	if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) == count($vacancy->skills)) {
		$only_go[] = $v;
	}
}
var_dump($only_go);

// get candidates that know one of these languages (elixir, erlang, f#, haskell, lisp)
$vacancy = new Vacancy([],['elixir', 'erlang', 'f#', 'haskell', 'lisp']);

$at_least_one = [];
foreach ($candidates as $k => $v) {
	if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) > 0) {
		$at_least_one[] = $v;
	}
}
var_dump($at_least_one);

/**
 * OPTION 2: Through a matching function.
 * This is a more flexible option, which can be scaled depending on needs. 
 * This solution would be better suited to serve queries from variables (i.e. sent through url parameters, etc.), ideally responfin to a JS call.
 * However, it's probably less efficient than the previouse verbose solution, as it performs array intersections and counting, even when these are not needed.
*/

/**
 * This function returns an array of matched candidates for a vacancy for given conditions.
 *
 * A vacancy instance, and an istance of candidates must be specified. 
 * Options are optional and will revert to default values. 
 *
 * @param object $vacancy This is an instance of Vacancy
 * 
 * @param array $candidates This is an instance of Candidates
 * 
 * @param array $options This is an associative array with the following keys:
 * 				array['options']
 *        					[salaryRangeOperator]   string Defines the filter for salary range. Options are: '<','>','<>'
 *        					[skillMatching] 	    string Defines the filter for skills. Options are:
 *        										    null (no skill matching needed, 'all' (need to match all skills), 'min'+(int) (at least n numbers of skills)         					
 *
 * @return array
 */
function match_candidates_to_vacancy(Vacancy $vacancy, Candidates $candidates, $options = []) {
	$matched_candidates = [];
	
	//Set default options if needed
	if (!array_key_exists('salaryRangeOperator', $options)) {
		$options['salaryRangeOperator'] = '<';
	}
	if (!array_key_exists('skillMatching', $options)) {
		$options['skillMatching'] = null;
	}
	
	//Construct filters from options
	$number_of_skill_matches = 0;
	if ($options['skillMatching'] == 'all') {
		$number_of_skill_matches = count($vacancy->skills);
	} else if (substr($options['skillMatching'], 0, 3) == 'min') {
		$number_of_skill_matches = (int) substr($options['skillMatching'], 3);
	}
	
	//Check if salary range is specified
	if (count($vacancy->salaryRange) > 0) {
		
		//Loop candidates and pupulate results
		foreach ($candidates as $k => $v) {
			
			//Case 1: Match if candidate salary is less than first salary figure
			if ($options['salaryRangeOperator'] == '<') {
				
				if ($v->candidate->salary < $vacancy->salaryRange[0]) {
					if ($number_of_skill_matches > 0) {
						if ($number_of_skill_matches == count($vacancy->skills)) {
							if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) == $number_of_skill_matches) {
								$matched_candidates[] = $v;
							}
						} else {
							if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) >= $number_of_skill_matches) {
								$matched_candidates[] = $v;
							}
						}
					} else {
						$matched_candidates[] = $v;
					}
				}
			}
			
			//Case 2: Match if candidate salary is more than first salary figure
			if ($options['salaryRangeOperator'] == '>') {
			
				if ($v->candidate->salary > $vacancy->salaryRange[0]) {
					if ($number_of_skill_matches > 0) {
						if ($number_of_skill_matches == count($vacancy->skills)) {
							if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) == $number_of_skill_matches) {
								$matched_candidates[] = $v;
							}
						} else {
							if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) >= $number_of_skill_matches) {
								$matched_candidates[] = $v;
							}
						}
					} else {
						$matched_candidates[] = $v;
					}
				}
			}
			
			//Case 3: Match if candidate salary is within salary range
			if ($options['salaryRangeOperator'] == '<>') {
					
				if ($v->candidate->salary > $vacancy->getMinSalary() 
						&& $v->candidate->salary < $vacancy->getMaxSalary()) {
					if ($number_of_skill_matches > 0) {
						if ($number_of_skill_matches == count($vacancy->skills)) {
							if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) == $number_of_skill_matches) {
								$matched_candidates[] = $v;
							}
						} else {
							if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) >= $number_of_skill_matches) {
								$matched_candidates[] = $v;
							}
						}
					} else {
						$matched_candidates[] = $v;
					}
				}
			}
		}
	//If no salary range is specified, filter oly on skills
	} else {
		
		foreach ($candidates as $k => $v) {
			if ($number_of_skill_matches > 0) {
				if ($number_of_skill_matches == count($vacancy->skills)) {
					if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) == $number_of_skill_matches) {
						$matched_candidates[] = $v;
					}
				} else {
					if (count(array_intersect($v->candidate->skills->languages, $vacancy->skills)) >= $number_of_skill_matches) {
						$matched_candidates[] = $v;
					}
				}
			} else {
				$matched_candidates[] = $v;
			}
		}
		
	}
	
	return $matched_candidates;
}

// get candidates that earn over than 40k
$earn_over_40k = match_candidates_to_vacancy(new Vacancy([40000]), $candidates, ['salaryRangeOperator' => '>']);
var_dump($earn_over_40k);

// get candidates that earn between 20 to 30k and know PHP or JS
$earn_between_20k_30k_php_js = match_candidates_to_vacancy(new Vacancy([20000,30000],['php','js']), $candidates, ['salaryRangeOperator' => '<>','skillMatching' => 'min1']);
var_dump($earn_between_20k_30k_php_js);

// get candidates that earn less than 42k and know Ruby and C
$earn_less_42k_ruby_c = match_candidates_to_vacancy(new Vacancy([42000],['ruby','php']), $candidates, ['salaryRangeOperator' => '<','skillMatching' => 'all']);
var_dump($earn_less_42k_ruby_c);

// get candidates that know go
$only_go = match_candidates_to_vacancy(new Vacancy([],['go']), $candidates, ['skillMatching' => 'all']);
var_dump($only_go);

// get candidates that know one of these languages (elixir, erlang, f#, haskell, lisp)
$at_least_one = match_candidates_to_vacancy(new Vacancy([],['elixir', 'erlang', 'f#', 'haskell', 'lisp']), $candidates, ['skillMatching' => 'min1']);
var_dump($at_least_one);

/**
 * OPTION 3: Similar function of OPTION 2, but integrated within the Class design.
 * Possibly a Matcher object with similar parameters of above.
 * Most likely the "cleaner" solution.
 */

?>
